#
# devenv-sdkman.Dockerfile
#
# From https://gitlab.com/isokissa/devenv-dockerfiles
# Docker image for development using tools available via
# sdkman (java, clojure, scala, gradle, ...)
#
FROM ubuntu:22.10
LABEL maintainer="pt_jr@yahoo.com"

ENV TZ=Europe/Helsinki \
    DEBIAN_FRONTEND=noninteractive

# this is supposed to be enough to install sdkman and other stuff, probably 
# no need to edit
RUN apt-get update
RUN apt-get install -y tzdata sudo curl git-core gnupg \
    locales bash wget rsync nano zip unzip && \
    locale-gen en_US.UTF-8 && \
    adduser --quiet --disabled-password --shell /bin/bash \
    --home /home/devuser --gecos "User" devuser && \
    echo "devuser:<a href="mailto://p@ssword1">p@ssword1</a>" | chpasswd && \
    usermod -aG sudo devuser

USER devuser
WORKDIR /home/devuser
SHELL ["/bin/bash", "-c"]
RUN curl -s "https://get.sdkman.io" | bash && \
    source "$HOME/.sdkman/bin/sdkman-init.sh"

# At this point, sdkman is available. Use "sdk install..." to install any tool you need

CMD ["bash"]
