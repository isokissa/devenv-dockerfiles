#
# devenv-node.Dockerfile
#
# From https://gitlab.com/isokissa/devenv-dockerfiles
# For development of node/npm applications
#
FROM ubuntu:latest
LABEL maintainer="pt_jr@yahoo.com"

ENV TZ=Europe/Helsinki \
    DEBIAN_FRONTEND=noninteractive

# this is supposed to be enough to install node and npm stuff, probably 
# no need to edit
RUN apt-get update && \
    apt-get install -y tzdata sudo curl git-core gnupg \
    locales nodejs bash wget rsync nano npm fonts-powerline && \
    locale-gen en_US.UTF-8 && \
    adduser --quiet --disabled-password --shell /bin/bash \
    --home /home/devuser --gecos "User" devuser && \
    echo "devuser:<a href="mailto://p@ssword1">p@ssword1</a>" | chpasswd && \
    usermod -aG sudo devuser

# you may want to expose different ports, depending on what servers you
# need to run within the container. Port 3000 is example for developing 
# react applications. Delete if not needed. 
EXPOSE 3000

USER devuser
CMD ["bash"]
